"use strict";

// Trainer object using object literals
const trainer = {
    name: "Ash Ketchum",
    age: "10",
    pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
    friends: {
        hoenn: ["May", "Max"],
        kanto: ["Brock", "Misty"]
    },
    talk: function() {
        console.log("Pikachu! I choose you!");
    }
}

// Display trainer info
console.log(trainer);

// Pokemon constructor
function Pokemon(name, level) {
    this.pokemonName = name;
    this.pokemonLevel = level;
    this.pokemonHealth = 2*level;
    this.pokemonAttack = level;

    // Tackle
    this.tackle = function(target) {
        if (target.pokemonHealth > 0) {
            console.log(this.pokemonName + " tackles " + target.pokemonName);

            target.pokemonHealth -= this.pokemonAttack; 
            console.log(target.pokemonName + "'s health is now reduced to " + target.pokemonHealth);
    
            // Checks if pokemon will faint after the attack
            if (target.pokemonHealth <= 0) {
                target.faint();
            }

        } else {
            console.log("Yamero! Target pokemon already fainted.");
        }
    }

    // Faint
    this.faint = function() {
        console.log(this.pokemonName + " fainted!");
    }

    // Splash
    this.splash = function() {
        console.log(this.pokemonName + " uses splash.");
        console.log("But nothing happened!");
    }
}

// Instantiate pokemon object
let shedinja = new Pokemon("Shedinja", 1);
let magneton =  new Pokemon("Magneton", 75);
let blastoise = new Pokemon("Blastoise", 100);
let amoonguss = new Pokemon("Amoonguss", 70);

// Display each pokemon info
console.log(shedinja);
console.log(magneton);
console.log(blastoise);
console.log(amoonguss);

// Sample Battle
magneton.tackle(shedinja);
console.log(shedinja);

blastoise.tackle(amoonguss);
blastoise.tackle(amoonguss);
console.log(amoonguss)

magneton.splash();